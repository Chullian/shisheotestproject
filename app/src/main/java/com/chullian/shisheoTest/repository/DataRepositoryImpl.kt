package com.chullian.shisheoTest.repository

import com.chullian.shisheoTest.data.model.responses.ResponseItem
import com.chullian.shisheoTest.data.network.ApiResult
import com.chullian.shisheoTest.data.network.ApiServices
import com.chullian.shisheoTest.data.network.safeApiCall
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Created by binMammootty on 07/12/2021.
 */


class DataRepositoryImpl @Inject constructor(
    private val api: ApiServices
) : DataRepository {
    override suspend fun getData(): ApiResult<List<ResponseItem>?> {
        val result = safeApiCall(Dispatchers.IO) {
            api.getData()
        }
        return result
    }

}