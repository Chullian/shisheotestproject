package com.chullian.shisheoTest.repository

import com.chullian.shisheoTest.data.model.responses.ResponseItem
import com.chullian.shisheoTest.data.network.ApiResult

/**
 * Created by binMammootty on 07/12/2021.
 */


interface DataRepository {
    suspend fun getData(): ApiResult<List<ResponseItem>?>
}