package com.chullian.shisheoTest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by binMammootty on 07/12/2021.
 */

@HiltAndroidApp
class App:Application()