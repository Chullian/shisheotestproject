package com.chullian.shisheoTest.data.network;

import com.chullian.shisheoTest.data.model.responses.ResponseItem
import retrofit2.http.GET

const val BASE_URL = "https://gateway-dev.shisheo.com/social/api/web/post/"


/**
 * Created by binMammootty on 07/12/2021.
 */
interface ApiServices {

    @GET("arina/test")
    suspend fun getData(): List<ResponseItem>?
}

