package com.chullian.shisheoTest.data.model.responses


import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/**
 * Created by binMammootty on 07/12/2021.
 */

@Keep
data class ResponseItem(
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("image_url")
    var imageUrl: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("offer")
    var offer: String? = null
)