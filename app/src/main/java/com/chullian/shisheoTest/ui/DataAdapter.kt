package com.chullian.shisheoTest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.chullian.shisheoTest.R
import com.chullian.shisheoTest.data.model.responses.ResponseItem
import kotlin.properties.Delegates

/**
 * Created by binMammootty on 07/12/2021.
 */


class DataAdapter :
    RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    var list: List<ResponseItem> by Delegates.observable(emptyList()) { property, oldValue, newValue ->
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View) :
        RecyclerView.ViewHolder(view) {
        val image: ImageView by lazy {
            view.findViewById<ImageView>(R.id.image)
        }
        val name: TextView by lazy {
            view.findViewById<TextView>(R.id.name)
        }
        val rating: RatingBar by lazy {
            view.findViewById<RatingBar>(R.id.rating)
        }
        val description: TextView by lazy {
            view.findViewById<TextView>(R.id.description)
        }
        val offer: TextView by lazy {
            view.findViewById<TextView>(R.id.offer)
        }
        var isShown = false
        fun onBind(item: ResponseItem, position: Int) {
            image.load(item.imageUrl)
            name.text = item.name

            description.text = item.description
            offer.text = item.offer
            if (position == 0)
                view.setOnClickListener {
                    if(!isShown) {
                        rating.visibility = VISIBLE
                        rating.rating = 5F
                    }
                    else rating.visibility = GONE
                    isShown = !isShown
                }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.onBind(item, position)
    }

    override fun getItemCount() = list.size
}