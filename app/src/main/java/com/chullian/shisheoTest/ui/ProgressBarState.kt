package com.chullian.shisheoTest.ui


/**
 * Created by binMammootty on 07/12/2021.
 */


sealed interface ProgressBarState {

    object Idle:ProgressBarState
    object Show:ProgressBarState

}
