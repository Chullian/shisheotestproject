package com.chullian.shisheoTest.ui


/**
 * Created by binMammootty on 07/12/2021.
 */


data class MainActivityState(
    val progressBarState:ProgressBarState = ProgressBarState.Idle,
    val dataState:DataState = DataState.Empty
)