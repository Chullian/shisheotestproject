package com.chullian.shisheoTest.ui

import com.chullian.shisheoTest.data.model.responses.ResponseItem


/**
 * Created by binMammootty on 07/12/2021.
 */


sealed interface DataState {
    object Empty : DataState
    data class Error(val message: String) : DataState
    data class DataItems(val list: List<ResponseItem>) : DataState

}
