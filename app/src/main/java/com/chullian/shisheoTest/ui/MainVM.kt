package com.chullian.shisheoTest.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chullian.shisheoTest.data.network.ApiResult
import com.chullian.shisheoTest.data.network.NetworkErrors.NETWORK_ERROR
import com.chullian.shisheoTest.repository.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by binMammootty on 07/12/2021.
 */

@HiltViewModel
class MainVM @Inject constructor(
    private val repository: DataRepository
) : ViewModel() {

    private val _state = MutableStateFlow(MainActivityState())
    val state = _state.asStateFlow()


    fun getData() = viewModelScope.launch {
        _state.value = _state.value.copy(
            progressBarState = ProgressBarState.Show)
        val result = repository.getData()
        when(result){
            is ApiResult.GenericError -> {
                _state.value = _state.value.copy(
                    progressBarState = ProgressBarState.Idle,
                    dataState = DataState.Error(result.errorMessage.toString())
                )
            }
            ApiResult.NetworkError -> {
                _state.value = _state.value.copy(
                    progressBarState = ProgressBarState.Idle,
                    dataState = DataState.Error(NETWORK_ERROR)
                )
            }
            is ApiResult.Success -> {
                _state.value = _state.value.copy(
                    progressBarState = ProgressBarState.Idle,
                    dataState = DataState.DataItems(result.value?: emptyList())
                )
            }
        }
    }
}