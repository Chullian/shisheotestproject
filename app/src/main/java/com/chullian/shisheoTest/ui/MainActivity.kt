package com.chullian.shisheoTest.ui

import android.os.Bundle
import android.view.View.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chullian.shisheoTest.R
import com.chullian.shisheoTest.data.model.responses.ResponseItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

/**
 * Created by binMammootty on 07/12/2021.
 */

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val vm by viewModels<MainVM>()

    private val progressLayout: ConstraintLayout by lazy {
        findViewById(R.id.progressLayout)
    }

    private val adapter by lazy {
        DataAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<RecyclerView>(R.id.mainReyclerView).apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
        vm.getData()
        observeChanges()
    }

    private fun observeChanges() = lifecycleScope.launchWhenCreated {
        vm.state.collect {
            showOrHideProgress(it.progressBarState)
            when (it.dataState) {
                DataState.Empty -> {}
                is DataState.Error -> showError(it.dataState.message)
                is DataState.DataItems -> populateList(it.dataState.list)
            }
        }
    }

    private fun showOrHideProgress(progressBarState: ProgressBarState) {
        when (progressBarState) {
            ProgressBarState.Idle -> {
                progressLayout.visibility = GONE
            }
            ProgressBarState.Show -> {
                progressLayout.visibility = VISIBLE
            }
        }
    }

    private fun populateList(list: List<ResponseItem>) {
        adapter.list = list
    }

    private fun showError(message: String) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Alert")
        alert.setMessage(message)
        alert.setPositiveButton(
            "OK"
        ) { dialog, _ -> dialog.dismiss() };
        alert.show()
    }
}