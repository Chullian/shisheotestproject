package com.chullian.shisheoTest.di

import com.chullian.shisheoTest.data.network.ApiServices
import com.chullian.shisheoTest.data.network.BASE_URL
import com.chullian.shisheoTest.data.network.NetworkErrors.NETWORK_TIMEOUT
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by binMammootty on 07/12/2021.
 */


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {




    @Singleton
    @Provides
    fun provideRetrofitService(): ApiServices {
        val client = OkHttpClient.Builder()
            .callTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .build()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiServices::class.java)
    }
}
