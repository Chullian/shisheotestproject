package com.chullian.shisheoTest.di

import com.chullian.shisheoTest.repository.DataRepository
import com.chullian.shisheoTest.repository.DataRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


/**
 * Created by binMammootty on 07/12/2021.
 */


@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryInjection {

    @Binds
    abstract fun bindRepository(
        repository: DataRepositoryImpl
    ): DataRepository

}
